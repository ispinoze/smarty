<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Smarty Demo</title>
</head>
<body>
{$msg} <br/>
{$msg|upper} <br/>

select a country:
<select name="select">
{html_checkboxes values=$country_keys output=$countries selected=$selectedCountries seperator='<br/>'}
</select>
<p/>
Day of departure:{html_select_date start_year='-10'} <br/>
Time of departure:{html_select_time}
<p/>

<form id="form1" name="form1" method="post" action="">

	Flight:<input type="text" id="company" value="{$flight->company}" />
	<br/>
	From:<input type="text" id="from" value="{$flight->from}" />
</form>

<p/>

<table width="100%" cellpadding="0" cellspacing="0">

<tr align="left">
	<th width="18%" scope="col">SSN</th>
	<th width="25%" scope="col">Courses</th>
	<th width="14%" scope="col">CREDITS</th>
	<th width="21%" scope="col">SCORE</th>
	<th width="22%" scope="col">COMMENT</th>
</tr>
{ section name=c loop=$course }
<tr bgcolor="{cycle values='#FFA,#FFF'}">
	<td>{$smarty.section.c.iteration}</td>
	<td>{$courses[c].name}</td>
	<td>{$courses[c].credit}</td>
	<td>{$courses[c].score}</td>
	<td>{if $courses[c].score gte 50}
		PASS
		{else}
		FAIL
		{/if}
		</td>
</tr>
{/section}
</table>
<p/>
created on:{$posted|date_format:"%A, %B %e,%Y"}]
</body>
</html>
