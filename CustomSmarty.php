<?php

require 'libs/Smarty.class.php';

define('REQUIRED_BASE_DIR', './');

class CustomSmarty extends Smarty {
	function __construct() {
		parent::__construct();
	
		setlocale(LC_ALL, 'tr_TR.UTF-8');
		setlocale(LC_TIME, "");
		$this->caching=true;
		
		$this->debugging=false;
		
		$this->cache_lifetime = 10;
		$this->setCacheDir(REQUIRED_BASE_DIR."/cache/");
		$this->setConfigDir(REQUIRED_BASE_DIR."/config/");
		$this->setTemplateDir(REQUIRED_BASE_DIR."/templates/");
		$this->setCompileDir(REQUIRED_BASE_DIR."/templates_c/");
		
		
	}
}